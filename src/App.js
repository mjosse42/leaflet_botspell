import React, { Component } from 'react'
import L from 'leaflet'
import _ from 'lodash'
import styled from 'styled-components'
import 'leaflet/dist/leaflet.css'
import icon from 'leaflet/dist/images/marker-icon.png';
import iconShadow from 'leaflet/dist/images/marker-shadow.png';
import geo_json from './convert.json'

let DefaultIcon = L.icon({
	iconUrl: icon,
	shadowUrl: iconShadow,
});

L.Marker.prototype.options.icon = DefaultIcon;

const Wrapper = styled.div`
	width: ${props => props.width};
	height: ${props => props.height};
`

export default class App extends Component {

	state = {
		center: [37.795, -122.394],
		zoom: 0
		// minZoom: -5,
		// zoomControl: false,
		// crs: L.CRS.Simple
	}

	setMap = () => {
		this.map = L.map('mapid', this.state)
	
		// L.tileLayer('https://fs.botspell.com/api/testbot/svg/r1JcxXwOI_edmj-20-dxf_1_svg', {
		// 	maxZoom: 50,
		// 	tileSize: 600,
		// 	zoomOffset: -1,
		// }).addTo(this.map);
	
		let imageUrl = 'https://fs.botspell.com/api/testbot/svg/r1JcxXwOI_edmj-20-dxf_1_svg'
		let imageBounds = [[0, 0], [1, 1]]
		L.imageOverlay(imageUrl, imageBounds).addTo(this.map);
		this.map.fitBounds(imageBounds)

		// L.geoJSON(geo_json, {
		// 	style: function (feature) {
		// 		return {color: feature.properties.color};
		// 	},
		// 	pointToLayer: function(geoJsonPoint, latlng) {
		// 		return L.marker(latlng)
		// 	}
		// }).bindPopup(function (layer) {
		// 	return layer.feature.properties.description;
		// }).addTo(this.map);
	}

	setLayer = () => {
		L.marker([0.4818641916992188, 0.3666289598632813]).addTo(this.map)
		// "x": 0.2818641916992188,
		// "y": 0.3666289598632813,
		// let svgElement = document.createElementNS("http://www.w3.org/2000/svg", "svg");
		// svgElement.setAttribute('xmlns', "http://www.w3.org/2000/svg");
		// svgElement.setAttribute('viewBox', "0 0 200 200");
		// svgElement.innerHTML = '<rect width="200" height="200"/><rect x="75" y="23" width="50" height="50" style="fill:red"/><rect x="75" y="123" width="50" height="50" style="fill:#0013ff"/>';
		// let svgElementBounds = [ [40.71, -74.22], [40.77, -74.12] ];
		// L.svgOverlay(svgElement, svgElementBounds).addTo(this.map);
	}
	
	componentDidMount () {
		this.setMap()
		this.setLayer() 
	}
	
	render () {
		return (
			<div style={{width: '600px', margin: 'auto', marginTop: '30px'}}>
				<Wrapper id='mapid' width='600px' height='500px' />
			</div>
		)
	}
}
